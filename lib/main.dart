import 'package:flutter/material.dart';
import 'package:listview_provider/list_provider.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (ctx) => ListProvider.instance,
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: MyHomePage(title: 'Flutter Demo Home Page'),
      ),
    );
  }
}

class MyHomePage extends StatelessWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

 ListProvider lp = ListProvider.instance;

/*

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 5;
*/

  void _incrementCounter(String s) {
    print('_incrementCounter $s');
    lp.addItem(s);
    /*setState(() {
      _counter++;
    });*/
  }

  void _decrementCounter(int i) {
    i = i-1;
   String s = i.toString();
    print('_decrementCounter $s');

    lp.removeItem(s);
    if(lp.listItems.isNotEmpty){

    }



   /* setState(() {
      _counter--;
    });*/
  }

  @override
  Widget build(BuildContext context) {
    var itemData = Provider.of<ListProvider>(context);
    final lisItems = itemData.listItems;
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              height: 150,
              width: 300,
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemBuilder: (context, index){
                  return Container(
                    height: 100,
                      width: 100,
                      color: Colors.deepPurple,
                      margin: EdgeInsets.all(5),
                      child: Center(child: Text(lisItems[index], style: TextStyle(fontSize: 30, color: Colors.white),)), );
                },
                itemCount: lisItems.length,
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                IconButton(
                    icon: Icon(Icons.remove), onPressed: () =>_decrementCounter(lisItems.length)),
                IconButton(icon: Icon(Icons.add), onPressed: () => _incrementCounter(lisItems.length.toString())),
              ],
            )
          ],
        ),
      ),
    );
  }
}
