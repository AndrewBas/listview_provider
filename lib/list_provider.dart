import 'package:flutter/material.dart';

class ListProvider with ChangeNotifier {

  ListProvider._privateConstructor();

  static final ListProvider _instance = ListProvider._privateConstructor();

  static ListProvider get instance => _instance;

  List<String> _listItems = [];

List<String> get listItems{
  return [..._listItems];
}

void addItem(String s){
  print('addItem $s');
  _listItems.add(s);
  notifyListeners();
}

void removeItem(String s){
  print('removeItem $s');
  _listItems.remove(s);
  notifyListeners();
}

}

/*List<String> getListItems() {
    for (var i = 0; i < 100; i++) {
      print(i.toString());
      listItems.add(i.toString());
      print(listItems[i]);
    }
    notifyListeners();
    return listItems;
  }*/